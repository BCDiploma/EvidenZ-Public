# Verifier endpoint

## Overview

The endpoint `/verifier` allows issuers to add a verifier on their website to check the validity of a URL.

The REST API endpoint described in this documentation is designed to accept POST requests with a JSON body containing one required parameter (the url to check) and one optional parameter (the ID of the issuer). The endpoint will respond with plain text, accompanied by different HTTP status codes depending on the outcome of the request.

## Request

### Method
- `POST`

### Headers
- Content-Type: `application/json`

### Request Body

The request body must be a valid JSON object with the following structure:

```json
{
    "url": "string",
    "issuer": integer
}
```

- `url` (string, required): The url to be checked. This parameter is mandatory and must be provided in the request body.
- `issuer` (integer, optional): The ID of the issuer. This parameter is optional and may be omitted in the request body.

## Responses

The endpoint will return a plain text response along with an appropriate HTTP status code.

### Success Response (HTTP Status Code: 200)

The URL is known to have been delivered by our services. But the document may no longer be valid. Check the text message for details:

```
Url is valid
Url is valid but certificate has expired
Url is valid but certificate has been disabled
Url is valid but certificate has been subject to the right of oblivion
Url is valid, but the certificate's issuer has not been verified yet
```

### Bad Request (HTTP Status Code: 400)

```
Given url is invalid
Given url does not contain key
```

### Forbidden (HTTP Status Code: 403)

```
Domain is invalid
Url is valid but not issued by given issuer
```

### Not found (HTTP Status Code: 404)

```
The key is unknown
```

## Example Usage

### cURL Example

```bash
curl -X POST https://api.bcdiploma.com/verifier \
     -H "Content-Type: application/json" \
     -d '{
           "url": "https://certificate.bcdiploma.com/check/01D387641C0A5B4704C01108BB30883B412DCE3A73CA04BF03EB51ED0C7786C4aEgvZCs2alI5SHk3VHFlUHcvNCtMc0dpRUZFUXQvSndtamNRZ05kSWthUU9vMWNM"
         }'
```

### Website Integration Example

A small integration example can be found [here](./verifier_example)


### Response

```
Url is valid
```

## Error Handling

In case of an error, the endpoint will provide a descriptive error message along with an appropriate HTTP status code to help identify and resolve the issue.

## Algorithm

1. The validity od the given URL is checked. If the URL is not a real URL, then error 400.
2. The key is extracted from the URL (search for pattern `\b[a-zA-Z0-9]{128}\b`). If no key is found, then error 400.
3. The existence of the key in our registry is checked. If the key is not found, then error 404.
4. The domain from the given url is verified against the different possibilities (bcdiploma.com, evidenz.io, 3videnz.com, or the issuer domain as defined in the template). If the domain is not valid, then error 403.
5. If the issuer is specified in request, then it is compared to the actual issuer of the document. If different, then error 403.
6. The status of the document is checked. In every case, the status code of the response is 200. But the text message may be different.
    * *Url is valid*
    * *Url is valid but certificate has expired*
    * *Url is valid but certificate has been disabled*
    * *Url is valid but certificate has been subject to the right of oblivion*
