document.getElementById('submitButton').addEventListener('click', function() {
    var inputValue = document.getElementById('inputField').value;

    fetch('https://api-demo.bcdiploma.com/verifier', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({url: inputValue, issuer: 2})
    })
    .then(response => response.text())
    .then(data => {
        var resultDiv = document.getElementById('result');
        resultDiv.textContent = 'Result : ' + data;
    })
    .catch(error => console.error('Error :', error));
});
