# Manual Access to the certificate's Blockchain Proofs

## Abstract

This documentation will allow you to consult by yourself a BCdiploma certificate and all its proofs of authenticity. If you’re new to the Blockchain, please first read the Q&A section of the certificate.
The blockchain guarantees by nature that the data has not been modified since its storage on the blockchain. So, the verification of the certificate consists in checking that:
1. you are in a trusted BCdiploma environment
2. the issuer is valid and has been authorized to issue this certificate

## Are you in a trusted BCdiploma environment?

It is important to note that the certificate data, and not a document, was recorded in a blockchain transaction. This transaction was authorized by BCdiploma's smartcontract and its framework: EvidenZ. We will check the validity of the transaction by clicking on the **Transaction** link on the **Blockchain** card of the certificate, and by performing the following operations:
* Check that you are on the trusted Blockchain Explorer https://etherscan.io. Etherscan.io is a transaction explorer dealing with the Ethereum blockchain mainnet, for production purposes
* Check that this blockchain matches the one indicated in the **Blockchain** field of the **Blockchain** card of the certificate. Note that ETH-mainnet corresponds to the Ethereum Blockchain  
* Check that the transaction has been authorized by the smart contract for the issuer. To do so, check that the transaction status **TxReceipt Status** is set to **Success**
* Check that the smartcontract is the same as the EvidenZ framework on which BCdiploma is built. This smart contract is associated with an ENS (Ethereum Name Service) specific to the BCdiploma environment: evidenz.eth.
Also, compare the **Contract Address** of the **Blockchain** card to the one indicated by the blockchain explorer of the ENS evidenz.eth: https://etherscan.io/address/evidenz.eth. Both addresses must be identical.

You are in a trusted BCdiploma Blockchain environment.

## Is the issuer valid?

The EvidenZ smart contract is a program whose code is impossible to tamper, and which guarantees that the data can only be published by an issuer authorized by BCdiploma after a KYB procedure. We will check the validity of this issuer directly in the smartcontract. To do so: 
* Open a new tab and go to https://mycrypto.com/contracts/interact, and select "BCdiploma - EvidenZ 0x..." item in the "Select Existing Contract" dropdown list
* Select the **IssuerAddress** function, and copy/paste the **Blockchain Address** of the **Issuer Identity** card into the **address** input field, then click on the **Access** button. 
* The returned value is the internal identifier of the issuer. Check that this uint256 value is the same as the one indicated to the left of the « / »  in the **Template ID** field of the certificate **Data** card
* Copy this identifier and select the **issuers** function, then paste it in the **uint256** field. Click on the **Access** button.
* Check that the **name/legalReference/intentDeclaration** fields match the **Name/Legal references/Declaration** of **intent information** indicated in the **Issuer Identity** card
* Check that the **host** field matches the domain name of the certificate’s URL indicated in your browser’s address bar, and also the **Consultation server** information of the **Issuer Identity** card
* Copy the **validatorID** identifier, corresponding to the institution that approved the issuer after a KYB procedure
* Select the **validators** function, then paste validatorID into the **uint256** input field. Then click on the **Access** button
* Check that the **name/validatorAddress/legalReference/webSite** fields match the **Name/Blockchain Address/Legal references/Website** informations indicated in the **Authorized By of the certificate** card

The certificate issuer is valid, and you are on an authentic BCdiploma certificate.