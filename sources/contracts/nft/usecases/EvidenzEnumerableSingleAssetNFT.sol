// SPDX-License-Identifier: MIT
pragma solidity ^0.8.18;

import {Ownable} from '@openzeppelin/contracts/access/Ownable.sol';
import {ERC721} from '@openzeppelin/contracts/token/ERC721/ERC721.sol';
import {ERC721Enumerable} from '@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol';
import {ERC721Burnable} from '@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol';

import {ERC721Base64URI} from '../token/ERC721/extensions/ERC721Base64URI.sol';
import {ERC721Renamable} from '../token/ERC721/extensions/ERC721Renamable.sol';
import {Evidenz} from '../domain/Evidenz.sol';
import {SingleAsset} from '../domain/SingleAsset.sol';
import {Premint} from '../workflow/Premint.sol';

contract EvidenzEnumerableSingleAssetNFT is
    Evidenz,
    SingleAsset,
    Premint,
    ERC721Base64URI,
    ERC721Enumerable,
    ERC721Burnable,
    ERC721Renamable
{
    constructor(
        string memory name_,
        string memory symbol
    ) ERC721(name_, symbol) ERC721Renamable(name_) {}

    function mint(bytes32[] calldata hashedPinCode) external onlyOwner {
        for (uint256 i; i < hashedPinCode.length; i++) {
            uint256 tokenId = _mint();
            hashedPinCodes[tokenId] = hashedPinCode[i];
        }
    }

    function claim(
        address to,
        uint256 tokenId,
        string calldata pinCode
    ) external virtual onlyOwner {
        _requireMinted(tokenId);
        _requirePinCode(tokenId, pinCode);
        _safeTransfer(owner(), to, tokenId, '');
    }

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(
        bytes4 interfaceId
    )
        public
        view
        virtual
        override(ERC721, ERC721Renamable, ERC721Enumerable, SingleAsset)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    /**
     * @dev See {IERC721Metadata-tokenURI}.
     */
    function tokenURI(
        uint256 tokenId
    )
        public
        view
        virtual
        override(ERC721, ERC721Base64URI)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    /**
     * @dev See {ERC721-name}.
     */
    function name()
        public
        view
        virtual
        override(ERC721, ERC721Renamable)
        returns (string memory)
    {
        return super.name();
    }

    /**
     * @dev See {ERC721-_beforeTokenTransfer}.
     */
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 firstTokenId,
        uint256 batchSize
    ) internal virtual override(ERC721, ERC721Enumerable) {
        super._beforeTokenTransfer(from, to, firstTokenId, batchSize);
    }

    /**
     * @dev See {ERC721-_requireMinted}.
     */
    function _requireMinted(
        uint256 tokenId
    ) internal view override(ERC721, Evidenz) {
        return super._requireMinted(tokenId);
    }
}
