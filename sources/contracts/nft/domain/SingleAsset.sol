// SPDX-License-Identifier: MIT
pragma solidity ^0.8.18;

import {ERC721} from '@openzeppelin/contracts/token/ERC721/ERC721.sol';
import {Ownable} from '@openzeppelin/contracts/access/Ownable.sol';
import {Strings} from '@openzeppelin/contracts/utils/Strings.sol';
import {IERC165} from '@openzeppelin/contracts/utils/introspection/IERC165.sol';

import {CustomTemplate} from './CustomTemplate.sol';
import {Evidenz} from './Evidenz.sol';
import {ISingleAsset} from './ISingleAsset.sol';
import {Environment} from '../utils/Environment.sol';

abstract contract SingleAsset is Evidenz, CustomTemplate, ISingleAsset {
    using Strings for uint256;
    using Environment for Environment.Endpoint;

    string public description;
    string public image;
    string public termsOfUse;

    function setDescription(string calldata description_) external onlyOwner {
        description = description_;
    }

    function setImage(string calldata image_) external onlyOwner {
        image = image_;
    }

    function setTermsOfUse(string calldata termsOfUse_) external onlyOwner {
        termsOfUse = termsOfUse_;
    }

    function getDescription() external view returns (string memory) {
        return description;
    }

    function getImage() external view returns (string memory) {
        return image;
    }

    function getTermsOfUse() external view returns (string memory) {
        return termsOfUse;
    }

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(
        bytes4 interfaceId
    ) public view virtual override(ERC721, IERC165) returns (bool) {
        return
            interfaceId == type(ISingleAsset).interfaceId ||
            super.supportsInterface(interfaceId);
    }

    /**
     * @dev See {Evidenz-_metadata}.
     */
    function _metadata(
        uint256 tokenId
    ) internal view override returns (bytes memory) {
        bytes memory prefix = abi.encodePacked(
            '{"name": "',
            name(),
            ' #',
            tokenId.toString(),
            '","description": "',
            description,
            ' Augmented NFT Experience at ',
            _getExternalUrl(tokenId),
            '","image": "',
            image,
            '","external_url": "',
            _getExternalUrl(tokenId),
            '"'
        );
        bytes memory suffix = abi.encodePacked(
            ',"status": "',
            _getStatus(tokenId),
            '"}'
        );
        if (bytes(termsOfUse).length == 0)
            return abi.encodePacked(prefix, suffix);
        else
            return
                abi.encodePacked(
                    prefix,
                    ',"terms_of_use": "',
                    termsOfUse,
                    '"',
                    suffix
                );
    }

    function _getExternalUrl(
        uint256 tokenId
    ) private view returns (string memory) {
        return
            template.reader.buildURL(
                string(
                    abi.encodePacked(
                        block.chainid.toHexString(),
                        '/',
                        uint256(uint160(address(this))).toHexString(20),
                        '/',
                        tokenId.toString()
                    )
                )
            );
    }

    function _getStatus(uint256 tokenId) private view returns (string memory) {
        if (ownerOf(tokenId) != owner()) return 'claimed';
        else return 'minted';
    }
}
