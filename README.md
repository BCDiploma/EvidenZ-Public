
# EvidenZ-Public, BCdiploma & EvidenZ framework repository

## Introduction

EvidenZ is a generic framework allowing the certification of any type of certificate, with BCdiploma as first use case, dedicated to academic documents.

EvidenZ allows to deploy registers on the blockchain for all types of high value data. It is intended to enable institutions, companies and universities to issue certified data over the long term, while respecting the regulations concerning personal data (GDPR, and especially the “right to be forgotten”). It provides individuals with a direct and permanent access to data through a simple URL, freely accessible to all.

EvidenZ provides an innovative technical approach: certify the significant data instead of the document itself. This major innovation allows a large number of application fields.

Technical overview at https://www.EvidenZ.io.

As the first instance of use, BCdiploma develops a turnkey application for higher education institutions, and provides the graduate with a unique URL link: over his/her entire life, he/she will be able to prove the authenticity of his/her diploma in a single click.

Please find a [sample multilingual diploma](https://certificate.bcdiploma.com/check/701BDD33DB32AA94E487E2B78AE95374854BE5DC22BAD788A219AECA79036ED1RDQ5YWhhOU9vSTExRStWT1FWaFB0RGpDOXExeGdYcjhNS0xwRWN1M2xGK0plbG1Z) and another [sample certificate produced during the CES 2019](https://certificate.bcdiploma.com/#/check/FFD033F8F2E9604F78D426F860B5042FB92235E9523B89B34D7E8F8E732AC19EYVI3Z3E2TFNSR0h6YlBUSjlXamsyOXRtZzlUeWhXTVVwbEp4NDRXcmtpcThmOXRi).

## Technical advantages

BCdiploma stands out from all other blockchain players thanks to an innovative technical approach: certify the significant data instead of the documents.

* Immutability of the blockchain;
* Decentralised storage of data;
* Certification of data and their issuers through smart contract;
* Respecting personal data (GPRD);
* Public blockchain compatibility
* Portability to other blockchain ensured "by design";
* Customised UX and design for each certification.

## Open Source

To ensure the durability of the solution for users, the EvidenZ framework is Opensource. Combined with the transparency of a public blockchain, EvidenZ's opensource smart contracts and DApps allow BCdiploma customers to deploy the solution by themselves.

In order to protect EvidenZ's technological innovation, 2 initiatives are currently underway:
* A utility patent for the solution in USA has been deposited (September 2018), and the international search report relating to the registration of this patent in the USA [has been published by the European Patent Office on March, 2020](https://patents.justia.com/patent/20200099511). 
All 19 claims are positive, and the examiner approved the inventive step and novelty of the process presented, without finding any prior art. This will logically lead to a direct grant of the patent in Europe and then in the United States. The absence of objections throughout the entire text of the patent is statistically very rare and is a strong indication of BCdiploma’s technological leadership.
* Most of the sources are stored in a private Gitlab repository, and accessible only on request by BCdiploma customers.

Nevertheless, these sources may be transmitted under NDA to external auditors, at the sole discretion of Blockchain Certified Data SAS.

## A tokenized ecosystem

In the heart of EvidenZ is the BCDT token, a utility token to "reload" the "Number of certifications to be issued" of a school or company. The value created by BCD will be measured proportionally to the volume of on-chain data issued by the institutions. More information on the BCDT here: https://www.evidenz.io/bcdt-token.html
